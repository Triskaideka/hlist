<!doctype html>
<head>
<title>Pterosaur</title>
</head>
<body>

<p><strong>Pterosaurs</strong> were flying reptiles of the clade or order
<strong>Pterosauria</strong>. They existed from the late Triassic to the end of
the Cretaceous Period (220 to 65 million years ago). Pterosaurs are the earliest
vertebrates known to have evolved powered flight. Pterosaurs spanned a wide
range of adult sizes, from the very small Nemicolopterus to the largest known
flying creatures of all time, including Quetzalcoatlus and Hatzegopteryx.</p>

<p>Pterosaurs are often referred to in the popular media and by the general
public as flying dinosaurs, but this is incorrect. The term "dinosaur" is
properly restricted to only those reptiles descended from the last common
ancestor of the groups Saurischia and Ornithischia (clade Dinosauria, which
includes birds), and current scientific consensus is that this group excludes
the pterosaurs. Pterosaurs are also incorrectly referred to as pterodactyls,
particularly by journalists. "Pterodactyl" refers specifically to members of the
genus Pterodactylus, and more broadly to members of the suborder
Pterodactyloidea.</p>

<p>This document is an abrigded version of the <a
href="https://en.wikipedia.org/wiki/Pterosaur">Wikipedia entry on
Pterosaurs</a>.  It is used here under the terms of the <a
href="https://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License">Creative
Commons Attribution-ShareAlike 3.0 Unported License</a>, and may be reused under those same terms.</p>


<h1 id="description">Description</h1>

<p>The anatomy of pterosaurs was highly modified from their reptilian ancestors
for the demands of flight. Pterosaur bones were hollow and air-filled, like the
bones of birds. They had a keeled breastbone that was developed for the
attachment of flight muscles and an enlarged brain that shows specialised
features associated with flight. In some later pterosaurs, the backbone over the
shoulders fused into a structure known as a notarium, which served to stiffen
the torso during flight, and provide a stable support for the scapula (shoulder
blade).</p>

<h2 id="wings">Wings</h2>


<p>Pterosaur wings were formed by membranes of skin and other tissues. The
primary membranes attached to the extremely long fourth finger of each arm and
extended along the sides of the body to the ankles.</p>

<p>While historically thought of as simple, leathery structures composed of
skin, research has since shown that the wing membranes of pterosaurs were
actually highly complex and dynamic structures suited to an active style of
flight.</p>

<p>As evidenced by hollow cavities in the wing bones of larger species and soft
tissue preserved in at least one specimen, some pterosaurs extended their system
of respiratory air sacs (see Paleobiology section below) into the wing membrane
itself.</p>


<h3 id="wing_parts">Parts of the pterosaur wing</h3>

<p>The pterosaur wing membrane is divided into three basic units. The first,
called the propatagium ("first membrane"), was the forward-most part of the wing
and attached between the wrist and shoulder, creating the "leading edge" during
flight. The brachiopatagium ("arm membrane") was the primary component of the
wing, stretching from the highly elongated fourth finger of the hand to the hind
limbs. Finally, at least some pterosaur groups had a membrane that stretched
between the legs, possibly connecting to or incorporating the tail, called the
uropatagium.</p>

<p>A bone unique to pterosaurs, known as the pteroid, connected to the wrist and
helped to support a forward membrane (the propatagium) between the wrist and
shoulder. Evidence of webbing between the three free fingers of the pterosaur
forelimb suggests that this forward membrane may have been more extensive than
the simple pteroid-to-shoulder connection traditionally depicted in life
restorations.</p>

<p>Three lines of evidence, morphological, developmental and histological,
indicate that the pteroid is a true bone, rather than ossified cartilage. The
origin of the pteroid is unclear: it may be a modified carpal, the first
metacarpal, or a neomorph (new bone).</p>

<p>The pterosaur wrist consists of two inner (proximal) and four outer (distal)
carpals (wrist bones), excluding the pteroid bone, which may itself be a
modified distal carpal.</p>

<p>There has been considerable argument among paleontologists about whether the
main wing membranes (brachiopatagia) attached to the hind limbs, and if so,
where. Some fossils seem to demonstrate that the wing membrane did attach to the
hindlimbs, at least in some species. However, modern bats and flying squirrels
show considerable variation in the extent of their wing membranes and it is
possible that, like these groups, different species of pterosaur had different
wing designs.</p>

<p>Many, if not all, pterosaurs also had webbed feet.</p>


<h2 id="skull">Skull, teeth and crests</h2>

<p>Most pterosaur skulls had elongated jaws with a full complement of
needle-like teeth. Some advanced beaked forms were toothless, such as the
pteranodonts and azhdarchids, and had larger, more extensive, and more bird-like
beaks.</p>

<p>Unlike most archosaurs, which have several openings in the skull in front of
the eyes, in pterodactyloid pterosaurs the antorbital opening and the nasal
opening was merged into a single large opening, called the nasoantorbital
fenestra. This likely evolved as a weight-saving feature to lighten the skull
for flight.</p>

<p>Pterosaurs are well known for their often elaborate crests.</p>

<p>Since the 1990s, new discoveries and more thorough study of old specimens
have shown that crests are far more widespread among pterosaurs than previously
thought. In the cases of pterosaurs like Pterorhynchus and Pterodactylus, the
true extent of these crests has only been uncovered using ultraviolet
photography.</p>


<h2 id="pycnofibres">Pycnofibres</h2>

<p>At least some pterosaurs were covered with hair-like filaments known as
pycnofibres, similar to but not homologous (sharing a common structure) with
mammalian hair. Pycnofibres were not true hair as seen in mammals, but a unique
structure that developed a similar appearance through convergent evolution. The
presence of pycnofibres (and the demands of flight) imply that pterosaurs were
endothermic (warm-blooded).</p>


<h1 id="discovery">History of discovery</h1>

<p>The first pterosaur fossil was described by the Italian naturalist Cosimo
Collini in 1784. Georges Cuvier first suggested that pterosaurs were flying
creatures in 1801, and coined the name "Ptero-dactyle" in 1809 for the specimen
recovered in Germany.</p>


<h1 id="paleobiology">Paleobiology</h1>

<h2 id="flight">Flight</h2>

<p>The mechanics of pterosaur flight are not completely understood or modeled at
this time.</p>

<p>In 1985, the Smithsonian Institution commissioned aeronautical engineer Paul
MacCready to build a half-scale working model of Quetzalcoatlus northropi. It
flew several times in 1986 and was filmed as part of the Smithsonian's IMAX film
<i>On the Wing</i>.</p>


<h2 id="respiration">Air sacs and respiration</h2>

<p>A 2009 study showed that pterosaurs had a lung-air sac system and a precisely
controlled skeletal breathing pump, which supports a flow-through pulmonary
ventilation model in pterosaurs, analogous to that of birds. The presence of a
subcutaneous air sac system in at least some pterodactyloids would have further
reduced the density of the living animal.</p>


<h2 id="nervous">Nervous system</h2>

<p>A study of pterosaur brain cavities using X-rays revealed that the animals
(Rhamphorhynchus muensteri and Anhanguera santanae) had massive flocculi. The
flocculus is a brain region that integrates signals from joints, muscles, skin
and balance organs.</p>

<p>The pterosaurs' flocculi occupied 7.5% of the animals' total brain mass, more
than in any other vertebrate.</p>

<p>The flocculus sends out neural signals that produce small, automatic
movements in the eye muscles. These keep the image on an animal's retina steady.
Pterosaurs may have had such a large flocculus because of their large wing size,
which would mean that there was a great deal more sensory information to
process.</p>


<h2 id="ground">Ground movement</h2>

<p>Pterosaurs' hip sockets are oriented facing slightly upwards, and the head of
the femur (thigh bone) is only moderately inward facing, suggesting that
pterosaurs had a semi-erect stance. It would have been possible to lift the
thigh into a horizontal position during flight as gliding lizards do.</p>

<p>There was considerable debate whether pterosaurs ambulated as quadrupeds or
as bipeds.</p>

<p>Unlike most vertebrates, which walk on their toes with ankles held off the
ground (digitigrade), fossil footprints show that pterosaurs stood with the
entire foot in contact with the ground (plantigrade), in a manner similar to
humans and bears.</p>

<p>Though traditionally depicted as ungainly and awkward when on the ground, the
anatomy of at least some pterosaurs (particularly pterodactyloids) suggests that
they were competent walkers and runners.</p>


<h2 id="predators">Natural predators</h2>

<p>Pterosaurs are known to have been eaten by theropods. In the 1 July 2004
edition of <i>Nature</i>, paleontologist Eric Buffetaut discusses an early
Cretaceous fossil of three cervical vertebrae of a pterosaur with the broken
tooth of a spinosaur embedded in it. The vertebrae are known not to have been
eaten and exposed to digestion, as the joints still articulated.</p>


<h2 id="reproduction">Reproduction and life history</h2>

<p>Very little is known about pterosaur reproduction, and pterosaur eggs are
very rare. The first known pterosaur egg was found in the quarries of Liaoning,
the same place that yielded the famous 'feathered' dinosaurs. The egg was
squashed flat with no signs of cracking, so evidently the eggs had leathery
shells, as in modern lizards. A study of pterosaur eggshell structure and
chemistry published in 2007 indicated that it is likely pterosaurs buried their
eggs, like modern crocodiles and turtles.</p>

<p>Wing membranes preserved in pterosaur embryos are well developed, suggesting
pterosaurs were ready to fly soon after birth. Fossils of pterosaurs only a few
days to a week old (called flaplings) have been found, representing several
pterosaur families.</p>

<p>It is not known whether pterosaurs practiced any form of parental care, but
their ability to fly as soon as they emerged from the egg and the numerous
flaplings found in environments far from nests and alongside adults has led most
researchers to conclude that the young were dependent on their parents for a
relatively short period of time.</p>


<h2 id="activity">Daily activity patterns</h2>

<p>Comparisons between the scleral rings of pterosaurs and modern birds and
reptiles have been used to infer daily activity patterns of pterosaurs. The
pterosaur genera Pterodactylus, Scaphognathus, and Tupuxuara have been inferred
to be diurnal, Ctenochasma, Pterodaustro, and Rhamphorhynchus have been inferred
to be nocturnal, and Tapejara has been inferred to be cathemeral, being active
throughout the day for short intervals.</p>


<h1 id="evolution">Evolution and extinction</h1>

<h2 id="origins">Origins</h2>

<p>Because pterosaur anatomy has been so heavily modified for flight, and
immediate "missing link" predecessors have not so far been described, the
ancestry of pterosaurs is not well understood. Several hypotheses have been
advanced, including links to ornithodirans like Scleromochlus, an ancestry among
the basal archosauriforms like Euparkeria, or among the prolacertiformes (which
include gliding forms like Sharovipteryx).</p>


<h1 id="classification">Classification</h1>

<p>Classification of pterosaurs has historically been difficult, because there
were many gaps in the fossil record. Many new discoveries are now filling in
these gaps and giving a better picture of the evolution of pterosaurs.
Traditionally, they are organized into two suborders:</p>

<ul>
<li>Rhamphorhynchoidea: A group of early, basal ("primitive") pterosaurs, many
of which had long tails and short metacarpal bones in the wing. They appeared in
the Late Triassic period, and lasted until the late Jurassic.</li>

<li>Pterodactyloidea: The more derived ("advanced") pterosaurs, with short tails
and long wing metacarpals. They appeared in the middle Jurassic period, and
lasted until the Cretaceous-Paleogene extinction event wiped them out at the end
of the Cretaceous.</li>
</ul>


<h2 id="extinction">Extinction</h2>

<p>It was once thought that competition with early bird species might have
resulted in the extinction of many of the pterosaurs. By the end of the
Cretaceous, only large species of pterosaurs are known. The smaller species seem
to have become extinct, their niche filled by birds. However, pterosaur decline
(if actually present) seems unrelated to bird diversity. At the end of the
Cretaceous period, the Cretaceous-Paleogene extinction event which wiped out all
non-avian dinosaurs and most avian dinosaurs as well, and many other animals,
seemed to also take the pterosaurs. Alternatively, some pterosaurs may have been
specialised for an ocean-going lifestyle. Consequently, when the
Cretaceous-Paleogene extinction event severely affected marine life that these
pterosaurs fed on, they went extinct. However, forms like azhdarchids and
istiodactylids were not marine in habits.</p>

<p>Recently, several new pterosaur taxa have been discovered dating to the
Campanian/Maastrichtian, such as the ornithocheirids Piksi and "Ornithocheirus",
possible pteranodontids and nyctosaurids, and a tapejarid. This suggests that
late Cretaceous pterosaur faunas were far more diverse than previously thought,
possibly not even having declined significantly from the early Cretaceous.</p>


<h2 id="genera">Well known genera</h2>

<p>Examples of pterosaur genera include <i>Pteranodon</i>, <i>Pterodactylus</i>,
<i>Pterodaustro</i>, <i>Quetzalcoatlus</i>, <i>Ornithocheirus</i>,
<i>Hatzegopteryx</i>, and <i>Rhamphorhynchus</i>.</p>


<h1 id="popular">In popular culture</h1>

<p>Pterosaurs have been a staple of popular culture for as long as their cousins
the dinosaurs, though they are usually not featured as prominently in films,
literature or other art. Additionally, while the depiction of dinosaurs in
popular media has changed radically in response to advances in paleontology, a
mainly outdated picture of pterosaurs has persisted since the mid 20th
century.</p>

<p>While the generic term "pterodactyl" is often used to describe these
creatures, the animals depicted frequently represent either Pteranodon or
Rhamphorhynchus, or a fictionalized hybrid of the two.</p>


<h1 id="see_also">See also</h1>

<ul>
  <li><a href="http://en.wikipedia.org/wiki/Flying_and_gliding_animals">Flying and gliding animals</a></li>
  <li><a href="http://en.wikipedia.org/wiki/List_of_pterosaurs">List of pterosaurs</a></li>
  <li><a href="http://en.wikipedia.org/wiki/Mary_Anning">Mary Anning</a></li>
  <li><a href="http://en.wikipedia.org/wiki/Wyvern">Wyvern</a></li>
</ul>

</body>
</html>
